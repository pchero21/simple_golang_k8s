# Dockerfile References: https://docs.docker.com/engine/reference/builder/

FROM golang:latest

LABEL maintainer="Sungtae Kim <pchero21@gmail.com>"

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o main

EXPOSE 8090

CMD ["./main"]